package cz.cvut.fit.lejnajan;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class Weighted3SATGenerator {

    public static StringBuilder generateClause(int numberOfVariables) {
        StringBuilder sb = new StringBuilder();

        for (int j = 1; j <= numberOfVariables; j++) {

            if (Rng.instance().getRandomBoolean()) {
                sb.append('-');
            }
            sb.append(j + " ");
        }
        return sb;
    }

    public static StringBuilder generate3SATClause(int numberOfVariables) {
        StringBuilder sb = new StringBuilder();

        List<Integer> randomVariables = Rng.instance().getUniqueNumbersInRange(3, 1, numberOfVariables);

        for (int j = 0; j < 3; j++) {

            if (Rng.instance().getRandomBoolean()) {
                sb.append('-');
            }
            sb.append(randomVariables.get(j) + " ");
        }

        sb.append("0");

        return sb;
    }

    /**
     * Main class of generator.
     *
     * @param args args[0] expect number of variables in formula
     *             args[1] expect number of variables to number of clauses RATIO
     */
    public static void main(String[] args) throws IOException {
        if (args.length != 2) {
            System.err.println("args[0] expect number of variables in formula");
            System.err.println("args[1] expect RATIO of: number of clauses to number of variables");
            return;
        }

        int numberOfVariables = Integer.parseInt(args[0]);
        if (numberOfVariables < 3) {
            System.err.println("Can't create 3SAT clause with less than 3 variables!");
            return;
        }
        double ratio = Double.parseDouble(args[1]);
        int numberOfClauses = (int) Math.round(numberOfVariables * ratio);

        // prepare generated file
        String fileName = "3SAT_var" + numberOfVariables + "_cl" + numberOfClauses + ".cnf";
        PrintWriter writer = new PrintWriter(fileName, "UTF-8");
        // add comments
        writer.println("c " + fileName);
        writer.println("c " + "number of variables = " + numberOfVariables);
        writer.println("c " + "number of clauses = " + numberOfClauses);
        writer.println("c " + "clauses to variables ratio = " + (double) numberOfClauses / numberOfVariables);
        writer.println("c");

        // define cnf
        writer.println("p cnf " + numberOfVariables + " " + numberOfClauses);

        // define clauses
        StringBuilder sb = null;
        for (int i = 0; i < numberOfClauses; i++) {
            sb = generate3SATClause(numberOfVariables);
            writer.println(sb.toString());
        }

        // define weights
        final int MIN_VARIABLE_WEIGHT = 1;
        final int MAX_VARIABLE_WEIGHT = 99;
        sb = new StringBuilder();

        for (int i = 0; i < numberOfVariables; i++) {
            sb.append(Rng.instance().getRandomNumberInRange(MIN_VARIABLE_WEIGHT, MAX_VARIABLE_WEIGHT) + " ");
        }
        writer.println("w " + sb.toString());

        // exit
        writer.close();
    }
}


