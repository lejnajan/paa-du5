package cz.cvut.fit.lejnajan;

import java.util.*;

public class Rng {
    private static volatile Rng itsInstance = null;
    private Random rd;

    private Rng() {
        rd = new Random();
    }

    public static Rng instance() {
        synchronized (Rng.class) {
            if (itsInstance == null) {
                itsInstance = new Rng();
            }
        }
        return itsInstance;
    }

    public int getRandomNumberInRange(int min, int max) {
        return rd.nextInt((max - min) + 1) + min;
    }

    public boolean getRandomBoolean() {
        return getRandomNumberInRange(0, 1) == 1;
    }

    public List<Integer> getUniqueNumbersInRange(int howMany, int min, int max) {
        List<Integer> result = new ArrayList<>();

        Set<Integer> uniqueNumbers = new HashSet<>();
        while (uniqueNumbers.size() < howMany) {
            uniqueNumbers.add(getRandomNumberInRange(min, max));
        }

        for (Integer number :
                uniqueNumbers) {
            result.add(number);
        }
        return result;
    }
}