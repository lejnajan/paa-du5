package cz.cvut.fit.lejnajan;

import cz.cvut.felk.cig.jcop.problem.Configuration;
import cz.cvut.felk.cig.jcop.problem.sat.Clause;
import cz.cvut.felk.cig.jcop.problem.sat.SATFitness;
import cz.cvut.felk.cig.jcop.problem.sat.Variable;

public class MySAFitness extends SATFitness {
    private MySATProblem problem;

    private int numberOfVariables;
    private int numberOfClauses;
    private final int MAX_VARIABLE_WEIGHT = 100;
    private int maxVariablesWeight;

    private int clauseBonus;
    private int formulaBonus;

    public MySAFitness(MySATProblem problem) {
        super(problem);
        this.problem = problem;

        this.numberOfVariables = problem.getVariables().size();
        this.numberOfClauses = problem.getFormula().getClauses().size();

        this.maxVariablesWeight = numberOfVariables * MAX_VARIABLE_WEIGHT;

//        this.clauseBonus = maxVariablesWeight;
        this.clauseBonus = 100;
        this.formulaBonus = clauseBonus * 10;

        this.minFitness = 0;
        this.maxFitness = maxVariablesWeight + numberOfClauses * clauseBonus + formulaBonus;

        problem.setMinimalFitnessToBeASolution(numberOfClauses * clauseBonus + formulaBonus);
    }

    /**
     * Basically every fitness of any configuration is sum of weights of variables, that are true in configuration.
     * That's true even for configuration, that is not a solution.
     * <p>
     * Configuration that has X number of satisfied clauses are benefited X times clauseBonus.
     * Configuration that IS solution has obviously the best benefit. Moreover, it's fitness is benefited even more with formulaBonus.
     *
     * @param configuration current formula configuration
     * @return configuration fitness
     */
    @Override
    public double getValue(Configuration configuration) {
        double fitness = 0;

        int variablesWeight = 0;
        for (Variable variable : this.problem.getVariables()) {
            if (configuration.valueAt(variable.getIndex()) == 1)
                variablesWeight += variable.getWeight();
        }

        fitness += variablesWeight;

        int satisfiedClauses = 0;
        for (Clause clause : this.problem.getFormula().getClauses()) {
            if (clause.isTrue(configuration)) {
                satisfiedClauses++;
            }
        }

        fitness += satisfiedClauses * clauseBonus;

        if (satisfiedClauses == this.problem.getFormula().getClauses().size()) {
            fitness += formulaBonus;
        }

        return fitness;
    }

    // OLD IMPLEMENTATION
//    /**
//     * If formula with this configuration is satisfied,
//     * result is maximal possible weight - current weight (eg. configurations with lower weight has higher fitness).
//     * <p/>
//     * If not satisfied, result is number of non-satisfied clauses multiplied by -1.
//     *
//     * @param configuration current formula configuration
//     * @return configuration fitness
//     */
//    @Override
//    public double getValue(Configuration configuration) {
//        int remainingClauses = 0;
//        for (Clause clause : this.problem.getFormula().getClauses()) {
//            if (!clause.isTrue(configuration)) {
//                remainingClauses++;
//            }
//        }
//
//        if (remainingClauses > 0) {
//            return -1 * remainingClauses;
//        } else {
//            int totalVariablesWeight = 0;
//            for (Variable variable : this.problem.getVariables()) {
//                if (configuration.valueAt(variable.getIndex()) == 1)
//                    totalVariablesWeight += variable.getWeight();
//            }
//
//            return totalVariablesWeight;
//        }
//    }
}
