package cz.cvut.fit.lejnajan;

public class Stats {
    private int numberOfResults;
    private double ratio;

    public Stats() {
        numberOfResults = 0;
        ratio = 0;
    }

    public double getAverage() {
        return ratio / numberOfResults;
    }

    public void addResult(double result) {
        numberOfResults++;
        ratio += result;
    }

    public int getNumberOfResults() {
        return numberOfResults;
    }
}
