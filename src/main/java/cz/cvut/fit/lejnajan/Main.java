package cz.cvut.fit.lejnajan;

import cz.cvut.felk.cig.jcop.result.render.JFreeChartRender;
import cz.cvut.felk.cig.jcop.result.render.SimpleRender;
import cz.cvut.felk.cig.jcop.solver.Solver;
import cz.cvut.felk.cig.jcop.util.PreciseTime;
import org.apache.commons.io.FileUtils;

import java.io.*;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {
    public static void solveSatProblemUsingSimulatedAnnealing(MySATProblem satProblem, String[] args, boolean shouldPrintResults, boolean isVerbose) {
        satProblem.setLabel(satProblem.getLabel().substring(0, satProblem.getLabel().indexOf("_temp")));
        Solver solver;

        if (args.length == 4) {
            if (args[1].charAt(0) == 'N') {
                double constant = Double.parseDouble(args[1].substring(1));
                solver = new MySimpleSolver(new MySimulatedAnnealing(Double.parseDouble(args[3]), Double.parseDouble(args[2]),
                        Double.parseDouble(args[0]), (long) (satProblem.getDimension() * constant)), satProblem, isVerbose);
            } else {
                solver = new MySimpleSolver(new MySimulatedAnnealing(Double.parseDouble(args[3]), Double.parseDouble(args[2]),
                        Double.parseDouble(args[0]), Long.parseLong(args[1])), satProblem, isVerbose);
            }
        } else if (args.length == 3) {
            if (args[1].charAt(0) == 'N') {
                double constant = Double.parseDouble(args[1].substring(1));
                solver = new MySimpleSolver(new MySimulatedAnnealing(Double.parseDouble(args[2]),
                        Double.parseDouble(args[0]), (long) (satProblem.getDimension() * constant)), satProblem, isVerbose);
            } else {
                solver = new MySimpleSolver(new MySimulatedAnnealing(Double.parseDouble(args[2]),
                        Double.parseDouble(args[0]), Long.parseLong(args[1])), satProblem, isVerbose);
            }
        } else if (args.length == 1) {
            solver = new MySimpleSolver(new MySimulatedAnnealing(Double.parseDouble(args[0]), satProblem.getDimension()), satProblem, isVerbose);
        } else {
            throw new IllegalArgumentException("Main args exception!");
        }

        if (((MySimpleSolver) solver).isVerbose()) {
            solver.addListener(new JFreeChartRender(satProblem.getLabel() + "_" + "JFreeChartRender"));
            solver.addRender(new SimpleRender());
        }
        solver.run();
        solver.render();

        System.out.println("################################################################################################################################");
        if (shouldPrintResults) {
            printSolution(satProblem, System.out);
        }
    }

    public static File getResourceAsTempFile(int numberOfVariables, int numberOfClauses, int resourceSuffix) throws IOException {
        String prefix = "3SAT_var" + numberOfVariables + "_cl" + numberOfClauses;
        String suffix = ".cnf";
        if (resourceSuffix >= 0) {
            prefix = prefix + "-" + resourceSuffix;
        }
        String filePath = "/instances/" + prefix + suffix;

        InputStream is = Main.class.getResourceAsStream(filePath);

        if (is == null) {
            throw new IOException("Resource not found!");
        } else {
            System.out.println("");
            System.out.println("Opening resource with 3SAT instance definition: " + filePath);
        }

        return stream2TempFile(is, prefix, suffix);
    }

    public static File stream2TempFile(InputStream is, String filePrefix, String fileSuffix) throws IOException {
        final File tempFile = File.createTempFile(filePrefix + "_temp", fileSuffix);
        tempFile.deleteOnExit();
        FileUtils.copyInputStreamToFile(is, tempFile);

        return tempFile;
    }

    public static List<String> getAllInstancesFileNames() throws IOException {
        List<String> filenames = new ArrayList<>();
        try (InputStream in = Main.class.getResourceAsStream("/instances");
             BufferedReader br = new BufferedReader(new InputStreamReader(in))) {

            String resource;
            while ((resource = br.readLine()) != null) {
                filenames.add(resource);
            }
        }
        return filenames;
    }

    public static void solveAllAvailableInstances(String[] args) throws IOException {
        // solve all ./instances/*
        List<String> fileNames = getAllInstancesFileNames();

        Stats stats = new Stats();

        for (String fileName :
                fileNames) {
            Pattern regex = Pattern.compile("3SAT_var(\\d+)_cl(\\d+).*\\.cnf");
            Matcher matcher = regex.matcher(fileName);
            if (matcher.find()) {
                Pattern regexResource = Pattern.compile(".*-(\\d+).*");
                Matcher matcherResource = regexResource.matcher(fileName);
                MySATProblem satProblem;

                int numberOfVariables = Integer.parseInt(matcher.group(1));
                int numberOfClauses = Integer.parseInt(matcher.group(2));

                if (!matcherResource.find()) {
                    satProblem = new MySATProblem(getResourceAsTempFile(numberOfVariables, numberOfClauses, -1));
                } else {
                    satProblem = new MySATProblem(getResourceAsTempFile(numberOfVariables, numberOfClauses, Integer.parseInt(matcherResource.group(1))));
                }

                solveSatProblemUsingSimulatedAnnealing(satProblem, args, true, false);

                if (numberOfVariables >= 20) {
                    safeResultIfItIsMyOptimal(satProblem, stats);
                } else {
                    safeResultIfItIsMyOptimal(satProblem, null);
                }
            }
        }

        System.out.println("\nFound solutions was " + stats.getAverage() + "% worse than my already found optimal solutions! (average of " + stats.getNumberOfResults() + " instances)");
    }

    public static String printSolution(MySATProblem satProblem, PrintStream printStream) {
        StringBuilder solution = new StringBuilder();

        if (!satProblem.isBestConfigurationSolution()) {
            solution.append("No valid solution found!");
            return solution.toString();
        }

        solution.append("Fitness = " + (long) satProblem.getBestFitness() + ", Y = (");

        boolean[] bestConfiguration = satProblem.getBestConfiguration();
        for (int i = 0; i < bestConfiguration.length - 1; i++) {
            if (bestConfiguration[i]) {
                solution.append("1, ");
            } else {
                solution.append("0, ");
            }
        }
        if (bestConfiguration[bestConfiguration.length - 1]) {
            solution.append("1");
        } else {
            solution.append("0");
        }

        solution.append(")");

        if (printStream != null) {
            printStream.println(solution.toString());
        }

        return solution.toString();
    }

    public static void safeResultIfItIsMyOptimal(MySATProblem satProblem, Stats stats) throws IOException {
        Path file = Paths.get("./myOptimals/" + satProblem.getLabel() + ".optimum");

        try {
            Files.createFile(file);
            Files.write(file, printSolution(satProblem, null).getBytes()); // calculated for first time
            System.out.println("-> This instance was solved for the first time, created file " + file.getFileName().toString());
        } catch (FileAlreadyExistsException ignored) {
            BufferedReader bufferedReader = Files.newBufferedReader(file);
            String firstLine = bufferedReader.readLine();
            if (firstLine == null) {
                Files.write(file, printSolution(satProblem, null).getBytes()); // file exist but is corrupted
                return;
            }

            Pattern regexNoSolution = Pattern.compile("No valid solution found!");
            Pattern regexSolution = Pattern.compile("Fitness\\s+=\\s+(-)?(\\d+),\\s+Y\\s+=\\s+\\((.*)\\)");

            Matcher matcher = regexNoSolution.matcher(firstLine);
            if (matcher.find()) {
                if (printSolution(satProblem, null).equals("No valid solution found!")) {
                    System.out.println("-> No change. Still not valid solution.");
                } else {
                    Files.write(file, printSolution(satProblem, null).getBytes()); // better solution to be noted
                    System.out.println("-> FOUND SOLUTION IS FIRST VALID ONE!");
                }
            } else {
                matcher = regexSolution.matcher(firstLine);
                if (!matcher.find()) {
                    Files.write(file, printSolution(satProblem, null).getBytes()); // file exist but is corrupted
                    return;
                }

                long currentFitness = (long) satProblem.getBestFitness();
                long soFarOptimalFitness = Long.parseLong(matcher.group(2));

                if (printSolution(satProblem, null).equals("No valid solution found!")) {
                    System.out.println("-> No valid solution found this time.");
                    return;
                }

                if (currentFitness == soFarOptimalFitness) {
                    System.out.println("-> No change. Found solution is as good as my already found optimal solution.");
                    if (stats != null) {
                        stats.addResult(0);
                    }
                } else if (currentFitness > soFarOptimalFitness) {
                    Files.write(file, printSolution(satProblem, null).getBytes()); // better solution to be noted

                    System.out.println("-> FOUND SOLUTION IS " +
                            (double) (currentFitness - soFarOptimalFitness) / soFarOptimalFitness * 100 +
                            "% BETTER THAN ANY SOLUTION BEFORE!");
                    if (stats != null) {
                        stats.addResult((double) (soFarOptimalFitness - currentFitness) / soFarOptimalFitness * 100);
                    }
                } else {
                    System.out.println("-> Found solution is " +
                            (double) (soFarOptimalFitness - currentFitness) / soFarOptimalFitness * 100 +
                            "% worse than my already found optimal solution.");
                    if (stats != null) {
                        stats.addResult((double) (soFarOptimalFitness - currentFitness) / soFarOptimalFitness * 100);
                    }
                }
            }
        }
    }

    /***
     *
     * @param args
     * args[0] expect annealingCoefficient
     * args[1] expect internalLoop
     * args[2] expect targetTemperature
     * args[3] expect initialTemperature
     *
     * supported args.length:
     * 1) 1 - only annealingCoefficient, internalLoop is set by instance size, targetTemperature fixed 1, initialTemperature will be calculated
     * 2) 3 - set all except initialTemperature - it will be calculated
     * 3) 4 - set all - internalLoop can be "N<constant>" -> size of problem multiplied with constant will be used in SA as internalLoop
     *
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        /* select resource with 3SAT instance definition */
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter number of variables: ");
        int numberOfVariables = scanner.nextInt();
        System.out.print("Enter number of clauses: ");
        int numberOfClauses = scanner.nextInt();

        if (numberOfVariables < 0 || numberOfClauses < 0) {
            solveAllAvailableInstances(args);
            return;
        }

        /*--------------------------------------------------------------------------------------------------------------------*/
        /* parse & calculate */
        long timestampStart = PreciseTime.getCpuTimeNano();

        MySATProblem satProblem = new MySATProblem(getResourceAsTempFile(numberOfVariables, numberOfClauses, -1));
        solveSatProblemUsingSimulatedAnnealing(satProblem, args, true, true);

        long timestampEnd = PreciseTime.getCpuTimeNano();

        /*--------------------------------------------------------------------------------------------------------------------*/
        /* print output */
        System.out.printf("CPU time (Input parsing + Calculation + Print output):  \t[%7f ms]\n", ((double) timestampEnd - timestampStart) / 1000000);
        safeResultIfItIsMyOptimal(satProblem, null);
        System.out.println("################################################################################################################################");
        System.out.println("");
    }
}