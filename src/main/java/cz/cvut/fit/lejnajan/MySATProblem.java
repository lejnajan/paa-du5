package cz.cvut.fit.lejnajan;

import cz.cvut.felk.cig.jcop.problem.Fitness;
import cz.cvut.felk.cig.jcop.problem.ProblemFormatException;
import cz.cvut.felk.cig.jcop.problem.sat.SAT;
import cz.cvut.felk.cig.jcop.problem.sat.Variable;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class MySATProblem extends SAT {
    private double bestFitness;
    private boolean[] bestConfiguration;

    private double bestFitnessOriginal;
    private double minimalFitnessToBeASolution;

    public MySATProblem(File configFile) throws IOException, ProblemFormatException {
        super(configFile);
    }

    public List<Variable> getVariables() {
        return this.variables;
    }

    @Override
    public Fitness getDefaultFitness() {
        return new MySAFitness(this);
    }

    public double getBestFitness() {
        return bestFitness;
    }

    public void setBestFitness(double bestFitness) {
        this.bestFitness = bestFitness;
    }

    public boolean[] getBestConfiguration() {
        return bestConfiguration;
    }

    public void setBestConfiguration(boolean[] bestConfiguration) {
        this.bestConfiguration = bestConfiguration;
    }

    public void setMinimalFitnessToBeASolution(double minimalFitnessToBeASolution) {
        this.minimalFitnessToBeASolution = minimalFitnessToBeASolution;
    }

    public void setBestFitnessOriginal(double bestFitnessOriginal) {
        this.bestFitnessOriginal = bestFitnessOriginal;
    }

    public boolean isBestConfigurationSolution() {
        return bestFitnessOriginal >= minimalFitnessToBeASolution;
    }
}
