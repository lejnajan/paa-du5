package cz.cvut.fit.lejnajan;

import cz.cvut.felk.cig.jcop.algorithm.BaseAlgorithm;
import cz.cvut.felk.cig.jcop.algorithm.CannotContinueException;
import cz.cvut.felk.cig.jcop.algorithm.InvalidProblemException;
import cz.cvut.felk.cig.jcop.problem.Configuration;
import cz.cvut.felk.cig.jcop.problem.Fitness;
import cz.cvut.felk.cig.jcop.problem.ObjectiveProblem;
import cz.cvut.felk.cig.jcop.problem.Operation;
import cz.cvut.felk.cig.jcop.solver.message.MessageBetterConfigurationFound;
import cz.cvut.felk.cig.jcop.solver.message.MessageOptimize;
import cz.cvut.felk.cig.jcop.util.JcopRandom;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.InvalidParameterException;

public class MySimulatedAnnealing extends BaseAlgorithm {
    private static Logger LOG = LoggerFactory.getLogger(MySimulatedAnnealing.class);

    /* annealing schedule */
    private double initialTemperature; /* high temperature causes higher chance that algorithm accepts worse solution, but it will search state space more wide */
    private double targetTemperature; /* should be higher than 0 */
    private double annealingCoefficient; /* should be in range 0.800 - 0.999 */
    private long internalLoop; /* should be higher than 0
    - how many times algorithm will try to find another state with same temperature == equilibrium limit */

    private double currentTemperature;
    private Fitness fitnessCalculator;
    private double currentFitness;
    private Configuration currentConfiguration;

    private long stepsCounter;
    private long acceptingBetterConfCounter;
    private long acceptingWorseConfCounter;
    private long rejectingNewConfCounter;
    private long rejectingNewConfInRowCounter;

    private boolean calculateInitialTemperature;

    private MySimpleSolver solver;

    public MySimulatedAnnealing(double initialTemperature, double targetTemperature, double annealingCoefficient, long internalLoop) {
        this.initialTemperature = initialTemperature;
        this.targetTemperature = targetTemperature;
        this.annealingCoefficient = annealingCoefficient;
        this.internalLoop = internalLoop;
        this.calculateInitialTemperature = false;

        if (!areValidParameters()) {
            throw new InvalidParameterException();
        }
    }

    public MySimulatedAnnealing(double targetTemperature, double annealingCoefficient, long internalLoop) {
        this.annealingCoefficient = annealingCoefficient;
        this.internalLoop = internalLoop;
        this.targetTemperature = targetTemperature;
        this.calculateInitialTemperature = true;
        this.initialTemperature = 1; // initial temperature will be counted
    }

    public MySimulatedAnnealing(double annealingCoefficient, long instanceSize) {
        this(1.0, annealingCoefficient, instanceSize);
    }

    private double calculateWorseToBetterConfRatio() {
        if (acceptingBetterConfCounter == 0) {
            return Double.MIN_VALUE;
        }

        return (double) acceptingWorseConfCounter / acceptingBetterConfCounter;
    }

    private double calculateInitialTemperature() {
        /* 50% chance of accepting new WORSE configuration => newWorseConfCount / newBetterConfCount = 1.0 optimally */
        double FINAL_RATIO = 1.0;
        double RAPID_INCREASE_COEFFICIENT = 1.5;

        double worseToBetterConfRatio = calculateWorseToBetterConfRatio();

        while (worseToBetterConfRatio < FINAL_RATIO) {
            this.initialTemperature *= RAPID_INCREASE_COEFFICIENT;
            initCommon();

            try {
                while (true) {
                    optimize();
                }
            } catch (CannotContinueException ex) {
                worseToBetterConfRatio = calculateWorseToBetterConfRatio();
//                System.out.printf("%.5f\n", worseToBetterConfRatio);
            }
        }
        return this.initialTemperature;
    }

    private boolean areValidParameters() {
        if (initialTemperature < targetTemperature) {
            return false;
        }

        if (initialTemperature <= 0 || targetTemperature <= 0) {
            return false;
        }

        if (annealingCoefficient >= 1 || annealingCoefficient <= 0) {
            return false;
        }

        if (internalLoop <= 0) {
            internalLoop = 1;
        }

        return true;
    }

    @Override
    public void init(ObjectiveProblem objectiveProblem) throws InvalidProblemException {
        this.problem = objectiveProblem;

        initCommon();

        if (this.calculateInitialTemperature) {
            LOG.info("Initial temperature calculation started, that may take a while...");
            Logger logger = this.disableLogging();
            this.initialTemperature = calculateInitialTemperature();
            this.enableLogging(logger);
            LOG.info("Calculated initial temperature is {}, reinitializing and starting!", initialTemperature);
            this.calculateInitialTemperature = false;
            if (!areValidParameters()) {
                // initialTemperature could be now < targetTemperature
                this.targetTemperature = this.initialTemperature - 1;
                LOG.info("Target temperature now equals initial temperature - 1!");
                if (!areValidParameters()) {
                    throw new InvalidParameterException();
                }
            }

            init(this.problem); // reinitialize
        }
    }

    private void initCommon() {
        this.setLabel("sT=" + this.initialTemperature + ", eT=" + this.targetTemperature + ", A=" + this.annealingCoefficient + ", N=" + this.internalLoop);

        this.currentTemperature = initialTemperature;

        this.currentConfiguration = problem.getRandomConfiguration(); // could give invalid starting configuration!
        this.fitnessCalculator = problem.getDefaultFitness();
        this.currentFitness = fitnessCalculator.getValue(currentConfiguration);
        LOG.debug("Starting random configuration {}, fitness {}", this.currentConfiguration, this.currentFitness);

        this.bestConfiguration = currentConfiguration;
        this.bestFitness = currentFitness;

        this.stepsCounter = 0;
        this.acceptingBetterConfCounter = 0;
        this.acceptingWorseConfCounter = 0;
        this.rejectingNewConfCounter = 0;
        this.rejectingNewConfInRowCounter = 0;
    }

    private void coolDown() {
        this.currentTemperature *= annealingCoefficient;
    }

    private boolean isEquilibrium() {
        if (stepsCounter == internalLoop) {
            stepsCounter = 0;
            return true;
        } else {
            return false;
        }
    }

    private boolean isFrozen() {
        if (currentTemperature > targetTemperature) {
            return false;
        } else {
            return true;
        }
    }

    Configuration findNextConfigurationInNeighborhood() {
        // fetch random operation q
        Operation operation = this.problem.getOperationIterator(this.currentConfiguration).getRandomOperation();
        if (operation == null) {
            throw new CannotContinueException("Unable to get random operation");
        }

        // expand to new configuration
        Configuration newConfiguration = operation.execute(this.currentConfiguration);

        // calculate fitness
        double newFitness = this.fitnessCalculator.getValue(newConfiguration);

        if (newFitness > this.currentFitness) {  // new.isBetterThan(this)
            acceptNewBetterConfiguration(newConfiguration, newFitness);
            return newConfiguration;
        } else {
            // newConfiguration pass temperature test?
            double delta = this.currentFitness - newFitness;

            double expression = Math.exp(-delta / this.currentTemperature); // e^(-x) is in (0, 1> for x from (-inf, 0>
            double x = JcopRandom.nextDouble(); // <0.0, 1.0>

            if (x < expression) {
                acceptNewWorseConfiguration(newConfiguration, newFitness);
                return newConfiguration;
            } else {
                rejectNewConfiguration();
                return this.currentConfiguration;
            }
        }
    }

    private void acceptNewBetterConfiguration(Configuration newConfiguration, double newFitness) {
//        LOG.debug("Accepting better new configuration {}, fitness {}", newConfiguration, newFitness);
        setNewConfigurationActive(newConfiguration, newFitness);
        informSolverAboutNewAcceptedConfiguration();
        this.acceptingBetterConfCounter++;
    }

    private void acceptNewWorseConfiguration(Configuration newConfiguration, double newFitness) {
//        LOG.debug("Accepting yet worse new configuration {}, fitness {}", newConfiguration, newFitness);
        setNewConfigurationActive(newConfiguration, newFitness);
        informSolverAboutNewAcceptedConfiguration();
        this.acceptingWorseConfCounter++;
    }

    private void rejectNewConfiguration() {
//        LOG.debug("Rejecting new configuration {}, fitness {} - keeping configuration {}, fitness {}", newConfiguration, newFitness, this.currentConfiguration, this.currentFitness);
        this.rejectingNewConfCounter++;
        this.rejectingNewConfInRowCounter++;
    }

    private void setNewConfigurationActive(Configuration newConfiguration, double newFitness) {
        this.currentConfiguration = newConfiguration;
        this.currentFitness = newFitness;
        this.rejectingNewConfInRowCounter = 0;
    }

    private void informSolverAboutNewAcceptedConfiguration() {
        LOG.debug("New configuration {}, temperature {}, {}", this.currentFitness, this.currentTemperature, this.currentConfiguration);
        if (!calculateInitialTemperature) {
            this.solver.increaseSolutionCounter();
            this.solver.sendMessage(new MessageOptimize());
            this.solver.sendMessage(new MessageBetterConfigurationFound(this.currentConfiguration, this.currentFitness));
        }
    }

    private void doStep() {
        stepsCounter++;

        Configuration nextConfiguration = findNextConfigurationInNeighborhood();

        // if it is best, set it as best
        if (this.currentFitness > this.bestFitness) {
            LOG.debug("Better solution {}, {}", this.currentFitness, this.currentConfiguration);
            this.bestConfiguration = this.currentConfiguration;
            this.bestFitness = this.currentFitness;
        }
    }

    @Override
    public void optimize() throws CannotContinueException {
        if (isFrozen()) {
            LOG.info("Initial temperature was {}. Optimalization has ended!", initialTemperature);
            LOG.info("New conf accepted: worse/better ratio = {}, worse {}x, better {}x", (int) ((double) acceptingWorseConfCounter / acceptingBetterConfCounter * 100000) / 100000.0,
                    acceptingWorseConfCounter, acceptingBetterConfCounter);
            LOG.info("New conf rejected {}x, in row before finish {}x ", rejectingNewConfCounter, rejectingNewConfInRowCounter);
            throw new CannotContinueException("Finished - frozen"); // declaring local solution as final
        }

        while (!isEquilibrium()) {
            doStep();
        }

        coolDown();
    }

    public Logger disableLogging() {
        Logger previousLogger = LOG;
        LOG = new NoLogger();
        return previousLogger;
    }

    public void enableLogging(Logger logger) {
        LOG = logger;
    }

    public void setSolver(MySimpleSolver solver) {
        this.solver = solver;
    }
}
