package cz.cvut.fit.lejnajan;

import cz.cvut.felk.cig.jcop.algorithm.Algorithm;
import cz.cvut.felk.cig.jcop.algorithm.CannotContinueException;
import cz.cvut.felk.cig.jcop.problem.Configuration;
import cz.cvut.felk.cig.jcop.problem.ObjectiveProblem;
import cz.cvut.felk.cig.jcop.problem.Problem;
import cz.cvut.felk.cig.jcop.problem.sat.Variable;
import cz.cvut.felk.cig.jcop.result.ResultEntry;
import cz.cvut.felk.cig.jcop.solver.SimpleSolver;
import cz.cvut.felk.cig.jcop.solver.message.MessageSolverStart;
import cz.cvut.felk.cig.jcop.solver.message.MessageSolverStop;
import cz.cvut.felk.cig.jcop.util.PreciseTimestamp;

public class MySimpleSolver extends SimpleSolver {
    private boolean isVerbose;
    private int solutionCounter;

    private MySATProblem mySATProblem;

    public MySimpleSolver(Algorithm algorithm, Problem problem, boolean isVerbose) {
        super(algorithm, problem);
        this.mySATProblem = (MySATProblem) problem;
        this.isVerbose = isVerbose;

        if (!this.isVerbose) {
            this.logger = new NoLogger();
            ((MySimulatedAnnealing) algorithm).disableLogging();
        }

        ((MySimulatedAnnealing) algorithm).setSolver(this);
        solutionCounter = 0;
    }

    public boolean isVerbose() {
        return isVerbose;
    }

    private void addResultsIntoProblem(ResultEntry resultEntry) {
        this.mySATProblem.setBestFitnessOriginal(resultEntry.getBestFitness());

        Configuration configuration = resultEntry.getBestConfiguration();
        boolean[] configurationArray = new boolean[mySATProblem.getDimension()];
        for (int i = 0; i < mySATProblem.getDimension(); i++) {
            if (configuration.valueAt(i) == 1) {
                configurationArray[i] = true;
            } else {
                configurationArray[i] = false;
            }
        }
        this.mySATProblem.setBestConfiguration(configurationArray);

        int variablesWeight = 0;
        for (Variable variable : this.mySATProblem.getVariables()) {
            if (configuration.valueAt(variable.getIndex()) == 1)
                variablesWeight += variable.getWeight();
        }

        this.mySATProblem.setBestFitness(variablesWeight);
    }

    @Override
    public void render() {
        if (this.isVerbose) {
            super.render();
            System.out.println("");
        }

        addResultsIntoProblem(this.getResult().getResultEntries().get(0));
    }

    public void increaseSolutionCounter() {
        this.solutionCounter++;
    }

    /**
     * Applies one algorithm on one problem until any condition is met or exception is raised.
     * <p/>
     * This method just removes code duplicities since most solvers has identical this part of solving. It is not
     * obligatory to use this method however, change it if necessary (for example algorithm switching on simple
     * problem).
     *
     * @param objectiveProblem problem to be solved
     * @param algorithm        algorithm to be used during solving
     * @return result entry for this optimization
     */
    @Override
    protected ResultEntry optimize(ObjectiveProblem objectiveProblem, Algorithm algorithm) {
        PreciseTimestamp startPreciseTimestamp = null;
        Exception algorithmException = null;

        try {
            startPreciseTimestamp = new PreciseTimestamp();

            algorithm.init(objectiveProblem);
            this.sendMessage(new MessageSolverStart(algorithm, objectiveProblem));
            logger.info("Started optimize, {} on {}.", algorithm, objectiveProblem);

            do {
                algorithm.optimize();
                if (this.isConditionMet()) break;
            } while (true);

        } catch (CannotContinueException e) {
            logger.warn("Got exception {}.", e.getClass().getSimpleName());
            algorithmException = e;
        }

        this.sendMessage(new MessageSolverStop(algorithm, objectiveProblem));
        logger.info("Stopped optimize.");
        algorithm.cleanUp();
        return new ResultEntry(algorithm, objectiveProblem, algorithm.getBestConfiguration(), algorithm.getBestFitness(), this.solutionCounter, algorithmException, startPreciseTimestamp);
    }
}
