Usage:
mvn clean install
java -jar target/Weighted3SATSimulatedAnnealing-1.0-SNAPSHOT.jar 0.950 N10 1 500

Run Example 1:
Enter number of variables: 20
Enter number of clauses: 91
=> input file will be ./instances/3SAT_var20_cl91.cnf

Run Example 2 (special - solve ALL instances):
Enter number of variables: -1
Enter number of clauses: -1
=> inputs will be gradually all instances in folder ./instances
(stats are calculater for instances with nubmber of varialbes >= 20)
...

Copyright: Jan Lejnar

     * args[0] expect annealingCoefficient
     * args[1] expect internalLoop
     * args[2] expect targetTemperature
     * args[3] expect initialTemperature
     *
     * supported args.lenght:
     * 1) 1 - only annealingCoefficient, internalLoop is set by instance size, targetTemperature fixed 1, initialTemperature will be calculated
     * 2) 3 - set all except initialTemperature - it will be calculated
     * 3) 4 - set all - internalLoop can be "N<constant>" -> size of problem multiplied with constant will be used in SA as internalLoop